extern crate clap;
extern crate lightrunner;

const VERSION: &str = "0.0.0";

fn main() {
    let matches = clap::App::new("lightrunner")
        .version(VERSION)
        .author("Syver Storm-Furru <syver.storm.furru@gmail.com")
        .about("A physically based raytracing renderer")
        .arg(clap::Arg::with_name("CONFIG")
                 .short("c")
                 .long("config")
                 .takes_value(true)
                 .value_name("FILE")
                 .help("Sets a custom rendering configuration"))
        .arg(clap::Arg::with_name("INPUT")
                 .required(true)
                 .help("Sets the scene to render")
                 .index(1))
        .arg(clap::Arg::with_name("DEBUG")
                 .short("d")
                 .long("debug")
                 .help("Turns on debugging logs"))
        .get_matches();

    // Display welcome message
    println!("Welcome to lightrunner v{}!", VERSION);

    let config = matches.value_of("config").unwrap_or("default.conf");
    println!("Using rendering configuration: {}", config);
    let scene = matches.value_of("INPUT").unwrap();
    println!("Scene to be rendered: {}",
             matches.value_of("INPUT").unwrap());
    let debug = if matches.is_present("debug") {
        println!("Printing debug information");
        true
    } else {
        false
    };

    if let Err(ref e) = lightrunner::run(&config, &scene, &debug) {
        use std::io::Write;
        let stderr = &mut ::std::io::stderr();
        let errmsg = "Error writing to stderr.";
        writeln!(stderr, "Error: {}", e).expect(errmsg);

        for e in e.iter().skip(1) {
            writeln!(stderr, "Caused by: {}", e).expect(errmsg);
        }

        if let Some(backtrace) = e.backtrace() {
            writeln!(stderr, "Backtrace: {:?}", backtrace).expect(errmsg)
        }

        ::std::process::exit(1)
    }
}
