use nalgebra;
use nalgebra::{Point3, Vector3};

type Material = (f32, f32, f32);

pub struct Ray {
    origin: Point3<f32>,
    direction: Vector3<f32>,
}

impl Ray {
    pub fn new(origin: Point3<f32>, direction: Vector3<f32>) -> Self {
        Self {
            origin: origin,
            direction: direction,
        }
    }

    pub fn trace<G: Geometric + ?Sized + Sync>(
        &self,
        far_clipping_distance: f32,
        geometry: &Vec<Box<G>>,
        lights: &Vec<Light>,
    ) -> Color {
        for geo in geometry {
            let t = geo.intersect_distance(self.origin, self.direction);
            if t >= far_clipping_distance {
                continue;
            }
            let current_point = self.origin + (t * self.direction);
            if let Some(clr) = Ray::calculate_lighting(&current_point, lights, geo) {
                return clr;
            }
        }
        (0, 0, 0)
    }

        fn calculate_lighting<G: Geometric + ?Sized + Sync>(
        current_point: &Point3<f32>,
        lights: &Vec<Light>,
        geometry: &Box<G>,
    ) -> Option<Color> {
        let normal = geometry.get_normal(current_point);
        
        if !geometry.is_in_bounds(current_point) {
            return None;
        }

        let mut light_color = (0, 0, 0);
        for light in lights {
            let light_dir = nalgebra::normalize(&(light.origin - current_point));

            let darkness = {
                let out = nalgebra::dot(&light_dir, &normal);
                if out <= 0.05 {
                    0.0
                } else if out > 1.0 {
                    1.0
                } else {
                    out
                }
            };

            let (r, g, b) = geometry.get_material();

            light_color =
                (
                    ((light.color.0 as f32 * r * light.strength) * darkness) as u8 + light_color.0,
                    ((light.color.1 as f32 * g * light.strength) * darkness) as u8 + light_color.1,
                    ((light.color.2 as f32 * b * light.strength) * darkness) as u8 + light_color.2,
                );
        }
        return Some((light_color.0, light_color.1, light_color.2));
    }
}

pub type Color = (u8, u8, u8);

pub trait Geometric {
    fn intersect_distance(&self, ray_origin: Point3<f32>, ray_direction: Vector3<f32>) -> f32;
    fn get_normal(&self, point: &Point3<f32>) -> Vector3<f32>;
    fn is_in_bounds(&self, point: &Point3<f32>) -> bool;
    fn get_material(&self) -> Material;
}

pub struct Sphere {
    pub origin: Point3<f32>,
    radius: f32,
    material: Material,
}

impl Sphere {
    pub fn new(origin: Point3<f32>, radius: f32, material: Material) -> Self {
        Self {
            origin: origin,
            radius: radius,
            material: material,
        }
    }
}

impl Geometric for Sphere {
    fn intersect_distance(&self, ray_origin: Point3<f32>, ray_direction: Vector3<f32>) -> f32 {
        let l = nalgebra::distance(&self.origin, &ray_origin);
        let r = self.radius;
        let alpha = nalgebra::angle(&(self.origin - ray_origin), &ray_direction);
        f32::cos(alpha) * l - f32::sqrt(r.powi(2) - l * f32::sin(alpha))
    }

    fn get_normal(&self, point: &Point3<f32>) -> Vector3<f32> {
        let normal = point - self.origin;
        nalgebra::normalize(&normal)
    }

    fn is_in_bounds(&self, point: &Point3<f32>) -> bool {
        let point_sphere_distance = nalgebra::distance_squared(point, &self.origin);
        if point_sphere_distance <= self.radius.powi(2) {
            return true;
        }
        false
    }

    fn get_material(&self) -> Material {
        (self.material.0, self.material.1, self.material.2)
    }
}

pub struct Light {
    origin: Point3<f32>,
    strength: f32,
    color: Color,
}

impl Light {
    pub fn new(origin: Point3<f32>, strength: f32, color: Color) -> Self {
        Self {
            origin: origin,
            strength: strength,
            color: color,
        }
    }
}

pub struct Triangle {
    point0: Point3<f32>,
    point1: Point3<f32>,
    point2: Point3<f32>,
    plane: Plane,
    material: Material,
}

impl Triangle {
    pub fn new(
        point0: Point3<f32>,
        point1: Point3<f32>,
        point2: Point3<f32>,
        material: Material,
    ) -> Self {
        let normal = {
            let edge0 = point1 - point0;
            let edge1 = point2 - point0;

            nalgebra::normalize(&edge0.cross(&edge1))
        };

        Self {
            point0: point0,
            point1: point1,
            point2: point2,
            plane: Plane::new(point0, normal, material),
            material: material,
        }
    }
}

impl Geometric for Triangle {
    fn intersect_distance(&self, ray_origin: Point3<f32>, ray_direction: Vector3<f32>) -> f32 {
        self.plane.intersect_distance(ray_origin, ray_direction)
    }
    
    fn get_normal(&self, _: &Point3<f32>) -> Vector3<f32> {
        self.plane.normal
    }
    
    fn is_in_bounds(&self, point: &Point3<f32>) -> bool {
        let edge0 = self.point1 - self.point0;
        let edge1 = self.point2 - self.point1;
        let edge2 = self.point0 - self.point2;

        let point0_c = self.point0 - point;
        let point1_c = self.point1 - point;
        let point2_c = self.point2 - point;

        let point0_n = point0_c.cross(&edge0);
        let point1_n = point1_c.cross(&edge1);
        let point2_n = point2_c.cross(&edge2);

        if self.plane.normal.dot(&point0_n) < 0.0 {
            return false;
        } else if self.plane.normal.dot(&point1_n) < 0.0 {
            return false;
        } else if self.plane.normal.dot(&point2_n) < 0.0 {
            return false;
        }
        
        true
    }
    
    fn get_material(&self) -> Material {
        self.material
    }
}

pub struct Plane {
    point: Point3<f32>,
    normal: Vector3<f32>,
    material: Material,
}

impl Plane {
    pub fn new(point: Point3<f32>, normal: Vector3<f32>, material: Material) -> Self {
        Self {
            point: point,
            normal: normal,
            material: material,
        }
    }
}

impl Geometric for Plane {
    fn intersect_distance(&self, ray_origin: Point3<f32>, ray_direction: Vector3<f32>) -> f32 {
        let dn = self.normal.dot(&ray_direction);
        let x0_minus_o = self.point - ray_origin;
        let no = self.normal.dot(&x0_minus_o);

        no / dn
    }

    fn get_normal(&self, _: &Point3<f32>) -> Vector3<f32> {
        self.normal
    }

    fn is_in_bounds(&self, _: &Point3<f32>) -> bool {
        true
    }

    fn get_material(&self) -> Material {
        self.material
    }
}
