extern crate nom;
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate serde_derive;
extern crate nalgebra;
extern crate serde;
extern crate toml;
extern crate rayon;
extern crate image;

mod error;
mod configuration;
mod renderer;
mod primitives;

use error::*;
use configuration::Configuration;

pub fn run(config_name: &str, scene: &str, debug: &bool) -> Result<()> {
    let config = Configuration::new_from_file(config_name)?;
    println!("Loaded configuration.");
    renderer::render(&config);
    println!("Done!");
    Ok(())
}
