use std::fs::File;
use std::io::Read;

use toml;
use error::*;

#[derive(Debug, Deserialize)]
pub struct Configuration {
    pub film_config: FilmConfig,
}

impl Configuration {
    pub fn new_from_file(file_name: &str) -> Result<Configuration> {
        let mut buf = String::new();
        let mut file = File::open(file_name)?;
        file.read_to_string(&mut buf)?;
        let config: Self = toml::from_str(&buf)?;
        Ok(config)
    }
}

#[derive(Debug, Deserialize)]
pub struct FilmConfig {
    pub image_size: (u32, u32),
    pub far_clipping_plane: u32,
    pub anti_aliasing: bool,
}
