use nom::{IResult, space, digit};

named!(image_size<&[u8], (&[u8], &[u8]>, do_parse!(
    tag!("ImageSize") >>
    many1!(space) >>
    x: many1!(digit) >>
    many1!(space) >>
    y: many1!(digit) >>
    (x, y)
    ));

named!()