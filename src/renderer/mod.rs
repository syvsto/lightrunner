use nalgebra::{Vector3, Point3};
use configuration::Configuration;
use primitives::{Ray, Color, Sphere, Light, Plane, Geometric, Triangle};
use image::ImageBuffer;
use image;
use std::f32;
use rayon::prelude::*;

use std::path::Path;

const AA_SAMPLING: u32 = 2;
const FOV_ANGLE: f32 = 70.0;

struct Renderer {
    rays: Vec<Ray>,
    geometry: Vec<Box<Geometric + Sync>>,
    lights: Vec<Light>,
}

impl Renderer {
    fn new(rays: Vec<Ray>, geometry: Vec<Box<Geometric + Sync>>, lights: Vec<Light>) -> Self {
        Self {
            rays: rays,
            geometry: geometry,
            lights: lights,
        }
    }

    fn run(&self) -> Vec<Color> {
        let hits: Vec<Color> = self.rays
            .par_iter()
            .map(|ray| ray.trace(7.0, &self.geometry, &self.lights))
            .collect();

        hits
    }
}

pub fn render(config: &Configuration) {
    let x_size = config.film_config.image_size.0;
    let y_size = config.film_config.image_size.1;

    let aa = config.film_config.anti_aliasing;
    let ratio = x_size as f32 / y_size as f32;

    let geometry: Vec<Box<Geometric + Sync>> =
        vec![
        Box::new(Sphere::new(
            Point3::new(0.0, 1.5, -5.0),
            0.7,
            (0.5, 0.5, 0.5),
        )),
        Box::new(Sphere::new(
            Point3::new(0.0, 0.4, -2.0),
            0.4,
            (0.7, 0.5, 0.5),
        )),
        Box::new(Sphere::new(
            Point3::new(1.0, 1.2, -5.0),
            0.6,
            (1.0, 0.0, 0.0),
        )),
        Box::new(Sphere::new(
            Point3::new(1.0, -1.0, -5.0),
            0.5,
            (0.0, 1.0, 0.0),
        )),
        Box::new(Sphere::new(
            Point3::new(-1.0, -1.0, -5.0),
            0.7,
            (1.0, 1.0, 1.0),
        )),
        Box::new(Triangle::new(
            Point3::new(-0.3, 0.0, -2.0),
            Point3::new(0.1, 0.0, -2.0),
            Point3::new(0.0, 0.1, -2.0),
            (1.0, 0.0, 1.0),
        )),
    ];

    let lights = vec![
        Light::new(Point3::new(1.0, 0.0, 0.0), 0.6, (255, 0, 0)),
        Light::new(Point3::new(-1.0, 1.0, 0.0), 0.6, (0, 255, 0)),
        Light::new(Point3::new(-1.0, -1.0, 0.0), 0.6, (0, 0, 255)),
    ];

    println!("Ratio: {}", ratio);
    println!("Creating camera rays...");

    let mut camera_rays = Vec::new();

    if !aa {
        for i in 0..x_size {
            for j in 0..y_size {
                let dir = Vector3::new(
                    (FOV_ANGLE as f32 / 2.0).tan() * ratio *
                        (2.0 * (i as f32 / x_size as f32) - 1.0),
                    (FOV_ANGLE as f32 / 2.0).tan() *
                        (-2.0 * (j as f32 / y_size as f32) + 1.0),
                    -1.0,
                );
                let ray = Ray::new(Point3::new(0.0, 0.0, 1.0), dir.normalize());
                camera_rays.push(ray);
            }
        }
    } else {
        for i in 0..AA_SAMPLING * x_size {
            for j in 0..AA_SAMPLING * y_size {
                let dir = Vector3::new(
                    (FOV_ANGLE as f32 / 2.0).tan() * ratio *
                        (-2.0 * (i as f32 / (AA_SAMPLING * x_size) as f32) + 1.0),
                    (FOV_ANGLE as f32 / 2.0).tan() *
                        (-2.0 * (j as f32 / (AA_SAMPLING * y_size) as f32) + 1.0),
                    -1.0,
                );
                let ray = Ray::new(Point3::new(0.0, 0.0, 1.0), dir.normalize());
                camera_rays.push(ray);
            }
        }
    }

    println!("Preprocessing geometry...");
    // sort_z_level(&mut geometry);
    // geometry.reverse();

    println!("Tracing from camera...");
    let renderer = Renderer::new(camera_rays, geometry, lights);
    let hits = renderer.run();

    let mut img = ImageBuffer::new(x_size, y_size);

    for (x, y, pixel) in img.enumerate_pixels_mut() {
        if !aa {
            let color = hits[x as usize * y_size as usize + y as usize];
            *pixel = image::Rgb([color.0, color.1, color.2]);
        } else {
            let subpx0 = hits[(AA_SAMPLING * x) as usize * (AA_SAMPLING * y_size) as usize +
                                  (AA_SAMPLING * y) as usize];
            let subpx1 = hits[((AA_SAMPLING * x) + 1) as usize * (AA_SAMPLING * y) as usize +
                                  (AA_SAMPLING * y) as usize];
            let subpx2 = hits[(AA_SAMPLING * x) as usize * (AA_SAMPLING * y_size) as usize +
                                  ((AA_SAMPLING * y) + 1) as usize];
            let subpx3 = hits[((AA_SAMPLING * x) + 1) as usize * (AA_SAMPLING * y) as usize +
                                  ((AA_SAMPLING * y) + 1) as usize];


            let r = (subpx0.0 as u32 + subpx1.0 as u32 + subpx2.0 as u32 + subpx3.0 as u32) / 4;
            let g = (subpx0.1 as u32 + subpx1.1 as u32 + subpx2.1 as u32 + subpx3.1 as u32) / 4;
            let b = (subpx0.2 as u32 + subpx1.2 as u32 + subpx2.2 as u32 + subpx3.2 as u32) / 4;

            *pixel = image::Rgb([r as u8, g as u8, b as u8]);
        }

    }

    let _ = img.save(&Path::new("test.png")).unwrap();
}


// fn sort_z_level(objects: &mut Vec<Box<Geometric>>) {
//     objects.sort_by(|a, b| a.origin[2].partial_cmp(b.origin[2]).unwrap());
// }
