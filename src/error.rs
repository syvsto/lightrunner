use toml;

error_chain! {
    types {}
    links {}
    foreign_links {
        Io(::std::io::Error);
        Toml(toml::de::Error);
    }
    errors {}
}


