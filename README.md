# lightrunner

**A physically based raytracing renderer**

Lightrunner is a renderer written in Rust and based on the book
[Physically Based Rendering: From Theory to implementation](http://www.pbrt.org).
The point of writing the renderer is to learn more about rendering and computer graphics.

It is currently not in a usable state whatsoever.